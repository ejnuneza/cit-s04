package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;

	public void init() throws ServletException{
		System.out.println("************************************");
		System.out.println(" DetailsServlet has been initialized");
		System.out.println("************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		ServletContext srvContext = getServletContext();
		
		HttpSession session = req.getSession();
		
		String branding = srvContext.getInitParameter("branding");
		String firstname = System.getProperty("firstname");;
		String lastname = session.getAttribute("lastname").toString();;
		String contact = req.getParameter("contact");
		String email = srvContext.getAttribute("mail").toString();
		
			PrintWriter output = res.getWriter();
					output.println(
						"<h1>Welcome to PhoneBook</h1>"
								+"<p>Branding: " + branding+"</p>"
								+ "<p>First Name: " + firstname + "</p>"
								+ "<p>Last Name: " + lastname + "</p>"
								+ "<p>Contact: " + contact + "</p>"
								+ "<p>Email: " + email + "</p>");
	}
	
	public void destroy(){
		System.out.println("************************************");
		System.out.println(" DetailsServlet has been destroyed. ");
		System.out.println("************************************");
	}

}
